```bash
#!/bin/bash

export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin

cat /etc/apt/sources.list
nano /etc/apt/sources.list

# Add LDAP server address to /etc/hosts file if you don’t have an active DNS server in your network.
cat /etc/hosts
nano /etc/hosts
echo "192.168.20.170 ldapsvr" | sudo tee -a /etc/hosts


# Install the packages for ApacheDS
apt install -y apacheds
apt install -y ldap-utils
apt install -y git


# Services
systemctl status apacheds
systemctl restart apacheds

# /etc/hosts Server Configuration for example.com
# =====================================================================
127.0.0.1	localhost example.net
127.0.1.1	ldapsvr

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
# =====================================================================

# KRB5 configuration
cp /etc/krb5.conf /etc/krb5.backup
cat /etc/krb5.conf
nano /etc/krb5.conf
# =====================================================================
[libdefaults]
    default_realm = EXAMPLE.COM

[realms]
    EXAMPLE.COM = {
        kdc = example.net
        admin_server = example.net
    }

[domain_realm]
    .example.com = EXAMPLE.COM
    example.com = EXAMPLE.COM
# =====================================================================

# ldap/ldap.conf
cat /etc/ldap/ldap.conf
nano /etc/
# =====================================================================
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-master.example.com:666
URI    ldap://192.168.20.170:10389


#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
# =====================================================================

# Searching the log
cat /var/lib/apacheds/instances/default/log/apacheds-rolling.log

cat /var/log/syslog
cat /var/log/auth.log


# Clearing the log
dd if=/dev/null > /var/lib/apacheds/instances/default/log/apacheds-rolling.log

> /var/log/syslog
> /var/log/auth.log


# Uninstall
dpkg -P apacheds
