```bash

# Client Host Fortress
sudo apt install -y openjdk-11-jre-headless
wget http://muug.ca/mirror/apache-dist/directory/studio/2.0.0.v20180908-M14/ApacheDirectoryStudio-2.0.0.v20180908-M14-linux.gtk.x86_64.tar.gz
sudo apt install -y maven
sudo apt install -y libapache-directory-api-java

# Configurations same as client
