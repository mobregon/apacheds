```bash
#!/bin/bash
# https://kifarunix.com/configure-openldap-client-on-debian-9-stretch/
# https://www.tecmint.com/configure-ldap-client-to-connect-external-authentication/
# https://help.ubuntu.com/community/LDAPClientAuthentication


## Installing packages either *ldap or *ldapd
# sudo apt -y install sssd-ldap
#sudo apt -y install libnss-ldap
#sudo apt -y install libpam-ldap
sudo apt -y install libnss-ldapd
sudo apt -y install libpam-ldapd
sudo apt install -y krb5-user
sudo apt install -y libpam-krb5

# ==============================================================================
#dpkg-reconfigure libpam-ldap # The file for libpam-ldap.conf can manually be editet in /etc/libpam-ldap.conf, or by running:
# 1. Set LDAP URI- This can be IP address or hostname
#ldapi://192.168.20.170:10389
#ldapi://ldapsvr:10389
# 2. Set a Distinguished name of the search base
#dc=vcn,dc=bc,dc=ca
#dc=security,dc=example,dc=com
# 3. Select LDAP version 3
# 4. Select Yes for Make local root Database admin
# 5. Answer No for Does the LDAP database require login?
# 6. Set LDAP account for root, something like
#cn=admin,dc=vcn,dc=bc,dc=ca
#uid=admin,ou=system
# ==============================================================================

# Check for changes in the file.
cat /etc/auth-client-config/profile.d/ldap-auth-config
# ==============================================================================

# ==============================================================================

## hosts.conf            # Add LDAP server address to /etc/hosts file if you don’t have an active DNS server in your network.
cat /etc/hosts
sudo nano /etc/hosts
echo "192.168.20.170 ldapsvr example.net" | sudo tee -a /etc/hosts
# ==============================================================================

# ==============================================================================

## nsswitch.conf         # After the installation, edit /etc/nsswitch.conf and add ldap authentication to passwd and group lines.
cat /etc/nsswitch.conf
sudo nano /etc/nsswitch.conf
# passwd: compat systemd ldap
# group: compat systemd ldap
# shadow: compat
# ==============================================================================
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         compat systemd ldap krb5
group:          compat systemd ldap krb5
shadow:         compat ldap krb5
gshadow:        files

hosts:          files mdns4_minimal [NOTFOUND=return] dns myhostname 
networks:       files

protocols:      db files ldap krb5
services:       db files ldap krb5
ethers:         db files
rpc:            db files

netgroup:       nis
sudoers:        files ldap
# ==============================================================================

## ldap.conf             # Reconfigure /etc/ldap/ldap.conf
sudo cp /etc/ldap/ldap.conf /etc/ldap/ldap.backup
cat /etc/ldap/ldap.conf
sudo nano /etc/ldap/ldap.conf
# ==============================================================================
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-master.example.com:666

BASE   dc=example,dc=com
#URI    ldap://192.168.20.170:10389
URI    ldap://example.net:10389

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
#TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
# end of ldap/ldap.conf
# ==============================================================================

## The file for libnss-ldap can be manually editet in cat /etc/ldap.conf, or by running: #dpkg-reconfigure libnss-ldap
sudo cp /etc/ldap.conf /etc/ldap_conf.backup
cat /etc/ldap.conf
sudo nano /etc/ldap.conf
# ==============================================================================
###DEBCONF###
##
## Configuration of this file will be managed by debconf as long as the
## first line of the file says '###DEBCONF###'
##
## You should use dpkg-reconfigure to configure this file via debconf
##

#
# @(#)$Id: ldap.conf,v 1.38 2006/05/15 08:13:31 lukeh Exp $
#
# This is the configuration file for the LDAP nameservice
# switch library and the LDAP PAM module.
#
# PADL Software
# http://www.padl.com
#

# Your LDAP server. Must be resolvable without using LDAP.
# Multiple hosts may be specified, each separated by a 
# space. How long nss_ldap takes to failover depends on
# whether your LDAP client library supports configurable
# network or connect timeouts (see bind_timelimit).
#host 127.0.0.1

# The distinguished name of the search base.
base dc=example,dc=com

# Another way to specify your LDAP server is to provide an
uri ldapi://example.net:10389
# Unix Domain Sockets to connect to a local LDAP Server.
#uri ldap://127.0.0.1/
#uri ldaps://127.0.0.1/   
#uri ldapi://%2fvar%2frun%2fldapi_sock/
# Note: %2f encodes the '/' used as directory separator

# The LDAP version to use (defaults to 3
# if supported by client library)
ldap_version 3

# The distinguished name to bind to the server with.
# Optional: default is to bind anonymously.
#binddn cn=proxyuser,dc=padl,dc=com

# The credentials to bind with. 
# Optional: default is no credential.
#bindpw secret

# The distinguished name to bind to the server with
# if the effective user ID is root. Password is
# stored in /etc/ldap.secret (mode 600)
rootbinddn uid=admin,ou=system

# The port.
# Optional: default is 389.
port 10389
# . . .
# . . .
# ==============================================================================

## nslcd.conf            # libpam-ldapd configuration.
sudo cat /etc/nslcd.conf
sudo nano /etc/nslcd.conf
# ==============================================================================
# /etc/nslcd.conf
# nslcd configuration file. See nslcd.conf(5)
# for details.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldap://192.168.20.170:10389/

# The search base that will be used for all queries.
base dc=example,dc=com

# The LDAP protocol version to use.
ldap_version 3

# The DN to bind with for normal lookups.
#binddn cn=annonymous,dc=example,dc=net
binddn uid=admin,ou=system
bindpw secret

# The DN used for password modifications by root.
#rootpwmoddn cn=admin,dc=example,dc=com
rootpwmoddn uid=admin,ou=system
rootpwmodpw secret

# SSL options
ssl off
tls_reqcert never
#tls_cacertfile /etc/ssl/certs/ca-certificates.crt

# The search scope.
#scope sub
# ==============================================================================

## common-auth
sudo cp /etc/pam.d/common-auth /etc/pam.d/common-auth.backup
cat /etc/pam.d/common-auth
sudo nano /etc/pam.d/common-auth
# ==============================================================================
#
# /etc/pam.d/common-auth - authentication settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authentication modules that define
# the central authentication scheme for use on the system
# (e.g., /etc/shadow, LDAP, Kerberos, etc.).  The default is to use the
# traditional Unix authentication mechanisms.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
auth	[success=4 default=ignore]	pam_krb5.so minimum_uid=10000 use_first_pass
auth	[success=3 default=ignore]	pam_unix.so nullok_secure try_first_pass
auth	[success=2 default=ignore]	pam_sss.so use_first_pass
auth	[success=1 default=ignore]	pam_ldap.so minimum_uid=10000 use_first_pass
# here's the fallback if no module succeeds
auth	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
auth	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
auth	optional			pam_cap.so 
# end of pam-auth-update config
# ==============================================================================

## common-account
sudo cp /etc/pam.d/common-account /etc/pam.d/common-account.backup
cat /etc/pam.d/common-account
sudo nano /etc/pam.d/common-account
# ==============================================================================
#
# /etc/pam.d/common-account - authorization settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authorization modules that define
# the central access policy for use on the system.  The default is to
# only deny service to users whose accounts are expired in /etc/shadow.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.
#

## 1. Kerberos configuration
account	required		pam_krb5.so minimum_uid=10000

## 2. ldap configuration #account	[success=ok new_authtok_reqd=done ignore=ignore user_unknown=ignore authinfo_unavail=ignore default=bad]	pam_ldap.so minimum_uid=10000
account	sufficient        pam_ldap.so minimum_uid=10000

## 3. Moved to third place. here are the per-package modules (the "Primary" block) #account	[success=1 new_authtok_reqd=done default=ignore]	pam_unix.so
account [success=1  default=ignore]     pam_unix.so

## 4. Pam requisite.  here's the fallback if no module succeeds
account	requisite			pam_deny.so

## 5. prime the stack with a positive return value if there isn't one already; this avoids us returning an error just because nothing sets a success code since the modules above will each just jump around
account	required			pam_permit.so

## 6. and here are more per-package modules (the "Additional" block)
account	sufficient			pam_localuser.so 

## 7. Disabling pam_sss.so
#account	[default=bad success=ok user_unknown=ignore]	pam_sss.so 

# end of pam-auth-update config
# ==============================================================================

## common-password    # Remove use_authtok on line 26 to look like below. password [success=1 user_unknown=ignore default=die] pam_ldap.so try_first_pass
sudo cp /etc/pam.d/common-password /etc/pam.d/common-password.backup
cat /etc/pam.d/common-password
sudo nano /etc/pam.d/common-password
# ==============================================================================
#
# /etc/pam.d/common-password - password-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define the services to be
# used to change user passwords.  The default is pam_unix.

# Explanation of pam_unix options:
#
# The "sha512" option enables salted SHA512 passwords.  Without this option,
# the default is Unix crypt.  Prior releases used the option "md5".
#
# The "obscure" option replaces the old `OBSCURE_CHECKS_ENAB' option in
# login.defs.
#
# See the pam_unix manpage for other options.

# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
password	requisite			pam_pwquality.so retry=3
password	[success=4 default=ignore]	pam_krb5.so use_first_pass minimum_uid=10000
password	[success=3 default=ignore]	pam_unix.so obscure use_authtok try_first_pass sha512
password	sufficient			pam_sss.so use_authtok
password	[success=1 default=ignore]	pam_ldap.so minimum_uid=10000 use_first_pass
# here's the fallback if no module succeeds
password	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
password	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
password	optional	pam_gnome_keyring.so 
# end of pam-auth-update config
# ==============================================================================

## common-session     # Enable creation of home directory on first login by adding the following line to the end of file /etc/pam.d/common-session
sudo cp /etc/pam.d/common-session /etc/pam.d/common-session.backup
cat /etc/pam.d/common-session
sudo nano /etc/pam.d/common-session
# session optional pam_mkhomedir.so skel=/etc/skel umask=077
# session required        pam_mkhomedir.so umask=0022 skel=/etc/skel
# ==============================================================================
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of sessions of *any* kind (both interactive and
# non-interactive).
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# The pam_umask module will set the umask according to the system default in
# /etc/login.defs and user settings, solving the problem of different
# umask settings with different shells, display managers, remote sessions etc.
# See "man pam_umask".
session optional			pam_umask.so
# and here are more per-package modules (the "Additional" block)
session	optional			pam_krb5.so minimum_uid=10000
session	required	pam_unix.so 
session	optional			pam_sss.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=10000
session	optional	pam_systemd.so 
session	optional			pam_mkhomedir.so skel=/etc/skel umask=022
# end of pam-auth-update config
# ==============================================================================

## Hide User List in Ubuntu 18.04 Login Screen
# 1. Run command to get access to root:
sudo -i

# 2. In the terminal, run command to allow gdm to make connections to the X server:
xhost +SI:localuser:gdm

# 3. Then switch to user gdm, which is required to run gsettings to configure gdm settings.
su gdm -l -s /bin/bash

# 4. Finally hide user list from login screen using Gsettings:
gsettings set org.gnome.login-screen disable-user-list true

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Restarting the NSCD service
sudo systemctl restart nscd
sudo systemctl enable nscd
sudo systemctl status nscd

# Test by switching to a user account on LDAP
su - username
su - hnelson

# ==============================================================================

## Uninstall




# ==============================================================================
## EXTRA
# Then configure the system to use LDAP for authentication by updating PAM configurations. 
# sudo pam-auth-update

# New. Remove use_authtok
#cat /usr/share/pam-configs/ldap
#sudo nano /usr/share/pam-configs/ldap
#Password: [success=end user_unknown=ignore default=die]	pam_ldap.so use_authtok try_first_pass

# Next, configure the LDAP profile for NSS by running.
#sudo auth-client-config -t nss -p lac_ldap


## host.conf
cat /etc/host.conf
sudo nano /etc/host.conf

## nscd.conf
cat /etc/nscd.conf
sudo nano /etc/nscd.conf

## pam.conf
cat /etc/pam.conf
sudo nano /etc/pam.conf


